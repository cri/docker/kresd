FROM debian:11-slim

ENV DEBIAN_FRONTEND=noninteractive

RUN apt-get update \
    && apt-get install -y \
              wget

RUN wget https://secure.nic.cz/files/knot-resolver/knot-resolver-release.deb \
    && dpkg -i knot-resolver-release.deb

RUN apt-get update \
    && apt-get install -y \
              knot-resolver

ENTRYPOINT ["/usr/sbin/kresd"]
CMD ["--noninteractive", "-c", "/etc/knot-resolver/kresd.conf"]
